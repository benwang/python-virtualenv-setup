python3 -m pip install virtualenv
if (!$?) {
    echo "Failed to install virtualenv! Exiting..."
    exit 1
}

python3 -m virtualenv venv
if (!$?) {
    echo "Failed to create virtual environment! Exiting..."
    exit 1
}

echo "Activating virtual environment..."
.\venv\scripts\activate
if (!$?) {
    echo "Failed to activate virtual environment! Exiting..."
    exit 1
}

echo "Installing required python packages..."

python -m pip install -r requirements.txt

if (!$?) {
    echo "Failed to install requirements!"
    exit 1
}

echo "Done!"