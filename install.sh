#!/bin/bash

# Check for Ubuntu
uname -v | grep Ubuntu > /dev/null
if [ $? != 0 ]; then
	echo "ERROR: OS is not Ubuntu. This script is only for Ubuntu"
	exit 1
fi

# Install Python 3
if ! type "python3" > /dev/null; then
	echo "No python3 found! Installing..."
	sudo apt install python3

	if [ $? != 0 ]; then
		echo "ERROR: Failed to install"
		exit 1
	fi
fi

# Install virtualenv
if ! type "virtualenv" > /dev/null; then
	echo "No virtualenv found! Installing"
	sudo apt install python3-virtualenv

	if [ $? != 0 ]; then
		echo "ERROR: Failed to install"
		exit 1
	fi
fi	

# Create virtual environment
if [ ! -d venv/ ]; then
	echo "venv not created yet. Creating..."
	virtualenv venv

	if [ $? != 0 ]; then
		echo "ERROR: Failed to create venv"
		exit 1
	fi
fi

# Activate virtual environment
. venv/bin/activate

# Install prereqs
pip3 install -r requirements.txt

echo Done!
