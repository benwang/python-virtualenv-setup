# Python Virtualenv setup

Base setup scripts for setting up and installing a virtual environment on different operating systems. Replace this README with whatever project you're working on!

## Setup

On Windows, run `setup.ps1` to install virtualenv, set up the venv, and install required python packages. On Linux, run `setup.sh`.

Rerun the script at any time to install updated packages from `requirements.txt`

## Activating the virtual requirement

### Windows

```bat
.\venv\Scripts\activate
```

### Linux

```bash
source venv/bin/activate
```

## Exporting packages

To update `requirements.txt`, run `pip freeze > requirements.txt`.